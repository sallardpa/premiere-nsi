# Premiere_NSI_Voltaire

Cours de Pierre-Alain Sallard pour l'enseignement de Première NSI au lycée Voltaire (Paris)

Les supports de cours et d'exercices sont fortements inspirés par ceux de nombreux collègues (liste non exhaustive) : 

* Vincent Bouillot, 
* Franck Chambon, 
* Gilles Lassus, 
* David Landry, 
* Romain Janvier,
* David Roche, 
* Frédéric Junier, 
* Johann Dolivet, 
* Frédéric Mandon, 
* etc.