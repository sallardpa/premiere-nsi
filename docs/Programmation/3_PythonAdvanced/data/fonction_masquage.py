def salut(nom) :
    message = "Bienvenue " # variable locale
    print(message + nom)
    return None

message = "Bonjour " # variable globale
salut("Alice")
print(message + "Alice")