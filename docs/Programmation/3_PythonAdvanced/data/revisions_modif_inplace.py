def bidule(tab) :
    tab.append(666) # on ajoute le nombre 666 à la fin du tableau passé en argument
    return None

monTableau = [1,2,3]
print("Avant passage dans la fonction : ", monTableau)
bidule(monTableau) # passage dans la fonction
print("Après passage dans la fonction : ", monTableau)