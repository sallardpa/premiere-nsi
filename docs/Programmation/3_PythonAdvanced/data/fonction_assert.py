def politesse(nom) :
    assert type(nom) == str, "Il faut saisir un texte"
    print("Bonjour " + nom)
    return None
