# Projet Turtle :turtle:



## ^^1. Découverte du module Turtle de Python^^ 

Le module (ou _bibliothèque_) `Turtle` fournit des commandes supplémentaires au langage Python de base, afin de pouvoir réaliser des dessins numériques.

 
### 1.1. Premier exemple

* Copier/coller le code ci-dessous dans un Éditeur Python (Thonny par exemple), puis l'exécuter.
* Expliquer le rôle de chacune des **fonctions** qui sont utilisées : `forward(x)`, `left(a)`, etc.

```python
from turtle import *

shape("turtle") # facultatif, c'est pour changer l'aspect du crayon
# début du dessin
forward(100)
left(90)
forward(70)

color("red")
circle(50,180)

color("black")
forward(70)

up()
goto(100,200)
down()

width(5)
fillcolor("yellow")
begin_fill()
circle(30,360)
end_fill()
# Terminer obligatoirement par ces lignes,
# qui permettent de laisser la fenêtre graphique ouverte jusqu'à ce qu'on la ferme avec un clic de la souris
exitonclick()
mainloop()
```


### 1.2. Deuxième exemple

* Copier/coller le code ci-dessous dans un Éditeur Python (Thonny par exemple), puis l'exécuter.
* Expliquer :
    - le principe de fonctionnement de la fonction `dessine_carre` ;
    - le rôle de la boucle `for` dans le corps du programme.

```python
from turtle import *

# Une fonction
def dessine_carre(taille, couleur):
    # taille est un entier qui donne la longueur en pixel du carré
    # couleur est une chaine de caractères qui donne la couleur de remplissage du carré
    fillcolor(couleur)
    begin_fill()
    for nombre_cote in range(4):
        forward(taille)
        left(90)
    end_fill()
    return None

# Départ à gauche de la fenêtre
up()
goto(-300,0)
down()

# Variables du corps de programme
longueur = 50
couleurA = "red"
couleurB = "blue"
utilise_couleurA = True

# Début de la frise
for compteur in range(6):
    if utilise_couleurA :
        dessine_carre(longueur, couleurA)
        utilise_couleurA = False # pour changer de couleur au prochain tour de boucle
    else :
        dessine_carre(longueur//2, couleurB)
        utilise_couleurA = True # pour changer de couleur au prochain tour de boucle
    up()
    forward(2*longueur)
    down()
    
# Terminer obligatoirement par ces lignes
exitonclick()
mainloop()

```

## ^^2. Exercices d'entrainement^^ 

Pour chacun des dessins ci-dessous, écrire un code qui permet de le reproduire.

!!! question "Un cristal"

    ![cristal](img/cristal.png)

!!! question "Des carrés grossissants"

    ![frise](img/huitcarres.png)

    Votre code devra comporter une fonction `carré(l)` dont le rôle est de dessiner un carré de côté `l`.

!!! question "Des chevrons tricolores"

    ![chevrons](img/friseTricolore.png)

    Votre code devra comporter une fonction `pointe(l, c)` dont le rôle est de dessiner une pointe de longueur `l` et de couleur `c`.


!!! question "Une spirale"
    Écrire une fonction `spirale(N)` qui, quand on l'appelle, dessine une spirale carrée composé de `N` segments.
    
    Le premier segment mesure 10 pixels et chaque segment suivant fait 10 pixels de plus que le précédent.
    
    Voici par exemple le résultat de l'appel `spirale(25)` :

    ![spirale](img/spirale.png)

## ^^3. Projet^^

Choisir et réaliser l'un des dessins présentés dans cette [liste de projets Turtle](Fiches_Projets_Turtle.pdf) en écrivant un code Python adéquat.



