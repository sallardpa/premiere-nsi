# Les algorithmes gloutons :boar:


## ^^1. Un problème classique : le voyageur de commerce^^

Une personne habitant à Paris doit visiter chacune des 93 préfectures de la France métropolitaine. 

Comment doit-elle organiser son parcours pour minimiser la distance totale parcourue ?

![carte_1](prefectures_carte2.png){: .center width=40%}


La seule façon de résoudre de manière précise ce problème est de procéder par **force brute**, c'est-à-dire de considérer tous les parcours possibles et de retenir celui qui offre la distance totale la plus courte.


!!! note "Nombre de parcours possibles"
    * S'il y a trois villes A, B et C à visiter en partant de Paris, il y a 3×2 = 6 parcours possibles qui sont :

        - Paris → A → B → C et Paris → A → C → B
        - Paris → B → A → C et Paris → B → C → A
        - Paris → C → A → B et Paris → C → B → A

    * S'il y a quatre villes A, B, C et D à visiter, on comprend avec la même logique qu'il y a 4×3×2 = 24 parcours possibles (écrivez-les !).

    * S'il y a 93 villes à visiter, il y aura donc 93×92×...×3×2 soit environ 10^144^ parcours possibles : un nombre à 145 chiffres ! 

    * En mathématiques, on utilise la notation 93! pour désigner le produit 93×92×...×3×2 et on l'appelle *factorielle de 93*. Ainsi de manière générale, la factorielle d'un entier N 
    est le nombre noté N! égal au produit N×(N-1)×...×3×2.

Mais ceci se révèle extrêmement couteux en temps de calcul dès que le nombre de villes est important : par exemple, s'il faut 1 microseconde pour calculer la distance totale d'un seul parcours reliant Paris aux 93 villes à visiter, il faudra plus de 93! = 10^144^ microsecondes, c'est-à-dire **10^130^ années**, pour tester tous les chemins possibles et trouver le meilleur chemin. Or l'univers n'existe que depuis 13 milliards d'années, et 13×10^9^ est bien plus petit que 10^130^ !!!

La **complexité** en temps de cette méthode par force brute est en $O(n!)$, :scream: pire qu'une croissance exponentielle !

![Classes de complexité](ordres-de-complexite.png){: .center width=40%}


??? danger "Une solution approchée"
    Le parcours ci-dessous semble raisonnable, mais rien ne garantit que ce soit le meilleur parcours !
    
    ![carte_2](prefectures_parcours.png){: .center width=40%}


## ^^2. Un algorithme glouton de recherche d'un parcours convenable^^

Puisqu'il n'est pas possible de trouver une solution exacte au problème ci-dessus en un temps raisonnable, l'idée est de mettre au point un algorithme qui va fournir une solution "acceptable" en un temps limité.

!!! tip "Principe de l'algorithme glouton de recherche d'un parcours convenable"
    On construit le parcours étape par étape selon le principe suivant : depuis une ville de départ, on regarde toutes les villes restant à visiter et on sélectionne celle qui est la plus proche. Et on recommence avec cette nouvelle ville.

    Puisqu'à chaque étape on a choisi la distance la plus courte, on peut espérer qu'au final la distance totale sera aussi la plus courte ... mais rien ne le garantit !


!!! question "Exercice 1"
    Appliquer cet algorithme pour trouver un parcours reliant O aux villes A, B, C, D et E. 
    
    ![](exo1.png){: .center}

    Est-il possible de trouver un autre trajet qui offre une distance total plus courte que le trajet fourni par l'algorithme glouton ?


??? danger "Solution Exercice 1"
    L'algorithme fourni le parcours O → C → B → A → D → E.

    ![](exo1_solution.png){: .center}

    On peut se convaincre en testant d'autres parcours que c'est bien le meilleur parcours possible.


!!! question "Exercice 2"
    Appliquer cet algorithme pour trouver un parcours reliant A aux huit autres villes. 
    ![](trajet1.png){: .center}

    Est-il possible de trouver un autre trajet qui offre une distance total plus courte que le trajet fourni par l'algorithme glouton ?


??? danger "Solution Exercice 2"
    L'algorithme fourni le parcours A → B → C → D → E → F → G → H → M.

    ![](trajet2.png){: .center}

    Mais à l'oeil nu, et avec le théorème de Pythagore si on veut une preuve mathématique, on trouve un autre parcours dont la distance totale est plus courte que le précédent : A → M → B → C → D → E → F → G → H.

    ![](trajet3.png){: .center}

    Ceci illustre le fait que l'algorithme donne une solution "acceptable" mais pas forcément optimale.


## ^^3.  Définition d'un algorithme glouton^^


Lors de la résolution d'un __problème d'optimisation__, la construction d'une solution se fait souvent de manière séquentielle, l'algorithme faisant à chaque étape un certain nombre de choix. 

 
!!! info "Définition"
    Le principe ***glouton*** consiste à faire le choix qui semble le meilleur sur le moment (choix local), sans se préoccuper des conséquences dans l'avenir, et sans revenir en arrière.

    Aveuglé par son appétit démesuré, __le glouton n'est pas sûr d'arriver à une solution optimale__, mais il fournit un __résultat rapidement__. 

Pour que la méthode gloutonne ait une chance de fonctionner, il faut que le choix local aboutisse à un **problème similaire plus petit** : c'est encore une illustration du principe "**diviser pour régner**". Chaque choix local successif permet d'avancer vers une solution globale "acceptable".

## ^^4.  QCM^^

*  __Question 1__


{{ multi_qcm(
    [
        "Parmi les phrases suivantes, laquelle est vraie ?",
        ["un algorithme glouton fournit toujours une solution optimale","un algorithme glouton est généralement moins complexe que les autres méthodes d'optimisation","un algorithme glouton étudie tous les cas possibles pour déterminer le cas optimal ","un algorithme glouton peut revenir en arrière en cas de blocage"],[2],
    ],
    shuffle = False)}}

*  __Question 2__

Un sherpa doit traverser la montagne pour vendre des marchandises dans le village voisin. Il ne peut transporter plus de 20kg dans son sac à dos et il dispose de 5 objets de poids différents et de valeurs différentes.

Voici cette liste d'objets sous forme de tuple `(nom de l'objet , valeurs en dollars , poids en kg)`.

```python
liste_objets=[('A',10,9),('B',7,12),('C',1,2),('D',3,7),('E',2,5)]
```

Il se demande quels objets choisir pour transporter la valeur totale maximale dans son sac tout en ne dépassant pas 20 kg.

Une méthode gloutonne consiste à :

- trier les objets par valeur (en $) décroissante
- considérer les objets un par un dans cet ordre et :

    - si cet objet ne fait pas dépasser le poids maximum autorisé du sac, on l'ajoute dans le sac ;
    - sinon on passe à l'objet suivant dans la liste triée.

Cet algorithme se termine quand on atteint la fin de la liste triée.

{{ multi_qcm(
    [
        "Quels objets va mettre le sherpa dans son sac s'il applique cet algorithme glouton dans la résolution de son problème ?",
        ["Objet 'A' puis objet 'D' puis objet 'C'","Objet 'A' puis objet 'D'","Objet 'B' puis objet 'D'","Objet 'B' puis objet 'D' puis objet 'C'"],[1],
    ],
    shuffle = False)}}
