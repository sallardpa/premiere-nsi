# Illustration du tri par insertion

from tkinter import *
from random import randint

largeur=1200
hauteur=600
police = "Helvetica "

detail_info = False
enPause = True
speed_animation = 5
speed_low = True

def tri_insertion(L):
    nbE=0
    rep=[(L[:],0,0)]
    rep.append((L[:],0,0))
    rep.append((L[:],0,0,nbE,"x"))
    rep.append((L[:],0,0,nbE,"b"))
    N = len(L)
    for i in range(1,N):
        rep.append((L[:],i,i,nbE,"x"))
        j = i-1
        idep=i
        while j>=0 and L[j] > L[j+1]:
            nbE+=1
            rep.append((L[:],j,idep,nbE,"a"))
            L[j+1],L[j]= L[j],L[j+1] # permutation
            rep.append((L[:],j,i,idep,nbE,""))
            nbE+=1
            rep.append((L[:],j,idep,nbE,"x"))
            j = j-1
        if j>=0:
            nbE+=1
        rep.append((L[:],j+1,i,nbE,"b"))
    rep.append((L[:],i,nbE,"e"))
    return rep

def modif_speed():
    global speed_animation, speed_low
    speed_low = not(speed_low)
    if speed_low :
        speed_animation = 5
    else :
        speed_animation = 1     
    return None

def show_info():
    global detail_info
    detail_info = not(detail_info)
    return None

def toogle_animation():
    global enPause
    if enPause :
        enPause = not(enPause)
    else :
        enPause = not(enPause)
    jouer()
    return None

def ListeNb():
    listenb=[]
    while len(listenb)!=16:
        nb=randint(-99,99)
        if nb not in listenb:
            listenb.append(nb)

    return listenb

def Partie():
    global rep,liste,rang
    global detail_info, enPause
    global speed_animation, speed_low

    bou1["state"]=DISABLED
    bou2["state"]=NORMAL
 
    listenb=ListeNb()
    rep=tri_insertion(listenb)
    liste=rep[0]
    rang=0
    jouer()
    return None

def jouer():
    global rep,liste,rang, enPause

    def bouger():
        global rang, rep, liste, enPause
        if enPause :
            dessiner(liste)
        else :
            can.delete("all")
            rang+=1
            if rang<len(rep):
                liste=rep[rang]
                dessiner(liste)
                can.after(100*speed_animation,bouger)
            else:
                liste=rep[len(rep)-1]
                dessiner(liste)
        return None
        
    def dessiner(liste):
        global rang,rep
        a=(largeur-200)/16
        H=(hauteur+a)/2
        H2=H-a

        def rectangle(i,Width,Fill,Outline):
            can.create_rectangle(100+a*i,hauteur/2,100+a*(i+1),hauteur/2+a,width=Width,fill=Fill,outline=Outline)
            return None
        def texte(i,H,Text,Fill):
            can.create_text(100+a/2+a*i,H,text=Text,fill=Fill,font=(police,str(hauteur//22),"bold"))
            return None
        
        def affichage_info(enCours):
            can.create_text(100+a/2+a*enCours,H-2*a,text="i = "+str(enCours),fill="red",font=(police,str(hauteur//22),"bold"))
            can.create_text(largeur/2,hauteur/5,text="comparaisons/échanges :  "+str(liste[-2]),fill="#DABEA0",font=(police,str(hauteur//18),"bold"))
            if liste[1] != liste[2]:
                can.create_text(largeur/2,hauteur/5+a/2,text="Boucle While ",fill="#DABEA0",font=(police,str(hauteur//18),"bold"))
            return None
        

        color2 = "grey60"
        color3 = "grey30"
        color4 = "black"
        color_not_sorted = "ivory2"
        color_sorted = "black"
        for i in range(16):
            rectangle(i,1,color2,color3)
            #texte(i,H,liste[0][i],"black")
            texte(i,H,liste[0][i],color_not_sorted)
            
        for i in range(16):
            if detail_info :
                affichage_info(liste[-3])

            if liste[-1] in ["","a","b","e"]:
                if i<=liste[1]:
                    rectangle(i,1,color3,color4)
                    texte(i,H,liste[0][i],color_sorted)

            if liste[-1]=="":
                if i==liste[1]:
                    rectangle(i,1,color2,color3)
                    texte(i,H2,liste[0][i],"red")
                elif i==liste[1]+1:
                    rectangle(i,1,color3,color4)
                    texte(i,H,liste[0][i],"green")
                    rectangle(i,3,"","green")
                elif i<=liste[2]:
                    rectangle(i,1,color3,color4)
                    texte(i,H,liste[0][i],color_sorted)
                rectangle(liste[1]+1,3,"","green")

            elif liste[-1]=="a":
                if i==liste[1]:
                    rectangle(i,1,color3,color4)
                    texte(i,H,liste[0][i],"green")
                    rectangle(i,3,"","green")
                elif i==liste[1]+1:# si on est au rang j
                    rectangle(i,1,color2,color3)
                    texte(i,H2,liste[0][i],"red")
                elif i<=liste[2]:
                    rectangle(i,1,color3,color4)
                    texte(i,H,liste[0][i],color_sorted)

            elif liste[-1]=="b":
                if i==liste[1]-1:
                    rectangle(i,1,color3,color4)
                    texte(i,H,liste[0][i],"green")
                elif i==liste[1]:
                    texte(i,H,liste[0][i],"red")
                elif i==liste[1]+1 and liste[1]<liste[2]:
                    rectangle(i,1,color3,color4)
                    texte(i,H,liste[0][i],color_sorted)
                    if i>1:
                        rectangle(i-2,3,"","green")
                elif i<=liste[2]:
                    rectangle(i,1,color3,color4)
                    texte(i,H,liste[0][i],color_sorted)
                rectangle(liste[1],3,"","maroon")

            elif liste[-1]=="x":
                if liste[1]==i:
                    texte(i,H,liste[0][i],"red")
                else:
                    if i<=liste[2]:
                        rectangle(i,1,color3,color4)
                        texte(i,H,liste[0][i],color_sorted)
    # Corps de la fonction
    bouger()
    return None

color2 = "grey60"
color3 = "grey30"
color4 = "black"
fen=Tk()
xmax = fen.winfo_screenwidth()
ymax = fen.winfo_screenheight()
x0 =  xmax/2-(largeur/2)
y0 =  ymax/2-(hauteur/2)
fen.geometry("%dx%d+%d+%d" % (largeur,hauteur, x0, y0))
tex1 = Label(fen, text="\n <<oo>>  tri par insertion  <<oo>>",font=(police,"35","bold"),fg=color4)
tex1.pack()
can=Canvas(height=hauteur-200,width=largeur)
can.pack()

bou1= Button(fen, text='TIRAGE ALEATOIRE', bg=color3, fg="white",font=(police,"25"),bd=0,command=Partie)
bou1.pack(side=LEFT,expand=YES, fill=BOTH)

bou2= Button(fen, text='RUN/PAUSE',  bg=color3, fg="white",font=(police,"25"),bd=0, state=DISABLED, command=toogle_animation)
bou2.pack(side= LEFT,expand=YES, fill=BOTH)

bou3= Button(fen, text='SPEED',  bg=color3, fg="white",font=(police,"25"),bd=0,command=modif_speed)
bou3.pack(side= LEFT,expand=YES, fill=BOTH)

# bou4= Button(fen, text='INFOS', bg=color3, fg="white",font=(police,"25"),bd=0,command=show_info)
# bou4.pack(side=LEFT,expand=YES, fill=BOTH)

fen.mainloop()


