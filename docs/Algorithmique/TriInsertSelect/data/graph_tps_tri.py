import matplotlib.pyplot as plt
from random import randint
from timeit import default_timer as timer 

# Algorithme du cours
def tri_selection(tab):
    for i in range(len(tab) - 1):
        indice_du_mini = i
        for j in range(i + 1, len(tab)) :
            if tab[j] < tab[indice_du_mini]:
                indice_du_mini = j
        # on intervertit alors tab[i] et tab[indice_du_mini]
        temp = tab[i] 
        tab[i] = tab[indice_du_mini]
        tab[indice_du_mini] = temp
    return tab

# Expérimentation
liste_taille = [100, 1000, 5000, 10000, 15000, 20000]

liste_temps = []

for taille in liste_taille :
    start = timer()
    tableau = [randint(1, 250) for k in range(taille)]
    tableau_trié = tri_selection(tableau) # type de tri retenu
    end = timer()
    liste_temps.append(end-start)

# Tracé du graphique
fig, ax = plt.subplots()
ax.plot(liste_taille, liste_temps, marker = "+",markersize=10)
plt.show()
