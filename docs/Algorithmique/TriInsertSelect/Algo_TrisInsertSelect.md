# Les tris de tableaux de données :fork_and_knife:

Il est très souvent nécessaire de trier des données, des objets.

Quelques exemples parmi d'autres :

- trier des mots par ordre alphabétique ;
- trier une liste de nombres du plus petit au plus grand ;
- trier des cartes par valeur et par couleur ;
- trier des évènements par ordre chronologique.

Tout comme lorsque nous venons de tirer des cartes dans un jeu de cartes, nous sommes face à des choix algorithmiques pour effectuer ce tri.

Plusieurs __algorithmes de tris classiques__ sortent du lot et nous apprendrons à en coder quelques uns (notés en gras ci-dessous) :

- __tri par sélection__ (en anglais : _selection sort_)
- __tri par insertion__ (en anglais : _insertion sort_)
- tri à bulles (simple mais hors programme)
- tri rapide "quicksort" (plus complexe et hors programme)
- tri fusion (encore un peu plus complexe et hors programme)


Une illustration en vidéo :
[Tri de cartes](https://www.youtube.com/watch?v=14c5N8CfnUo){target="_blank"}

> Remarque : seules nous concernent les parties "tri par insertion" (jusqu'à 0:45) et "tri par sélection" (de 0:45 à 1:30).

## ^^1. Le tri par insertion^^

### 1.1. Principe du tri par insertion

!!! tip ""
    On parcourt le tableau séquentiellement : chaque nouvelle valeur rencontrée va **s'insérer** au bon endroit dans la  partie gauche du tableau, celle qui est déjà triée. 

    Plus précisément : 
    
    * on considère successivement toutes les valeurs à trier, en commençant par celle en deuxième position ;
    * pour chacune des valeurs, on la compare aux valeurs situées à sa gauche, que l'on décale vers la droite, jusqu'à trouver la bonne position de la valeur à trier.

> __Remarque :__ les tris par insertion et par sélection (les deux tris que nous étudierons cette année) ont pour particularité de ne pas nécessiter la création d'un nouveau tableau . Ils __modifient sur place__ le tableau à trier.


### 1.2. Le tri par insertion animé !

* Voici une __animation simulant le fonctionnement de l'algorithme__ en partant de la liste `[6,5,3,1,8,7,2,4]` :  

![Animation du tri par insertion](data/Insertion-sort-example-300px.gif)

> _Remarque_ : le cadre rouge est placé sur le plus petit indice non trié.

* Une autre animation, sous forme de [programme Python à télécharger](data/animation_tri_insertion.py).

> _Remarque_ : lancer le programme, puis appuyer d'abord sur le bouton "Tirage aléatoire" et ensuite sur Run/Pause. Les variables `largeur` et `hauteur` du code permettent de modifier la taille de la fenêtre de cette animation.

### 1.3. Le tri par insertion par l'exemple

Le schéma ci-dessous illustre le principe de fonctionnement du tri par insertion pour le tableau `[27,10,12,8,11]`.

![nsi_prem_algo_insertion_1.png](data/nsi_prem_algo_insertion_1.png){: .center}

!!! faq "Exercice"
    Réalisez un schéma équivalent pour le tri par insertion du tableau `[14,22,3,17,8]`.

    _Important_ : bien mettre le "curseur" qui marque la valeur que l'on traite, ainsi que les flèches qui montrent les déplacements réalisés.

??? danger "Solution"
    
    ![](data/tri_insert_correction.png)

### 1.4. Algorithme du tri par insertion

L'algorithme du __tri par insertion__ écrit en pseudo-code est le suivant :

 
    Fonction tri_insertion(tableau T)
        n ← taille de T
        POUR i allant de 2 à n # numérotation des cases de 1 à n
            x ← T[i]
            # on va insérer x parmi les i premiers éléments
            j ← i
            TANT_QUE j-1 >= 1 et T[j-1] > x 
                # on décale les éléments vers la droite
                T[j] ← T[j-1]
                j ← j - 1
            FIN TANT_QUE
            T[j] ← x
        FIN POUR
        Renvoyer T # Facultatif : en réalité, le tableau T a été modifié sur place.

En voici une illustration, avec le même tableau `[27,10,12,8,11]` que précédemment, et où la valeur que l'on traite (_celle stockée dans la variable `x` de l'algorithme_) est inscrite en-dessous des cases du tableau :

![](data/tri-insertion_algo2_asy.png){: .center}

### 1.5. Programme Python du tri par insertion

À l'aide de l'algorithme ci-dessus, coder en Python un programme de tri par insertion.

{{ IDE("data/algo_tri_insert_vide") }}

??? danger "Solution"
    ```python
    def tri_insertion(tab):
        for i in range(1,len(tab)):
            x = tab[i]
            j = i
            while j-1 >= 0 and tab[j-1] > x  :
                tab[j] = tab[j-1]
                j = j-1
            tab[j] = x
        return tab
    ```


## ^^2. Le tri par sélection^^

### 2.1. Principe du tri par sélection

!!! tip ""
    On parcourt le tableau en **sélectionnant** l'élément "minimum" et on le déplace en début de tableau.
    
    On répète cette procédure en **sélectionnant** à chaque étape le prochain minimum dans le reste du tableau non trié et en plaçant ensuite ce minimum en tête de la zone non triée.

> Remarque : alors que le tri par insertion (§1) correspond à un tri de cartes où l'on découvre progressivement les cartes, le tri par sélection correspond à un tri de cartes où l'on voit dès le début toutes les cartes disponibles.

### 2.2.  Le tri par sélection animé !

* Voici une __animation simulant le fonctionnement de l'algorithme__ avec le tableau `[5, 4, 2, 1]`  :  

![Animation du tri par sélection](data/selection.gif){: .center}

> _Remarques :_
>- le cadre violet est placé sur le plus petit indice des cases non triées ;
>- le cadre vert est placé sur l'élément comparé, pour savoir s'il est le minimum de la partie non triée.

* Une autre animation, sous forme de [programme Python à télécharger](data/animation_tri_selection.py).


### 2.3.  Le tri par sélection par l'exemple

Le schéma ci-dessous illustre le principe de fonctionnement du tri par sélection pour le tableau `[12,8,23,10,15]`  :

![nsi_prem_algo_tri_select_1.png](data/nsi_prem_algo_tri_select_1.png){: .center}

!!! question "Exercice"
    Réalisez un schéma équivalent pour le tri par sélection du tableau `[14,22,3,17,8]`.

    :warning: _Attention_ : bien noter les deux curseurs `i` (qui marque le début des cases non triées) et `min` (qui marque la case du minimum de cette partie de tableau).

??? danger "Solution"

    ![](data/tri_select_correction.png)

    _Remarque_ : même si le tableau nous apparait comme trié après la 2^e^ étape, l'algorithme se poursuit jusqu'à ce que le curseur `i` atteigne l'avant-dernière case.

### 2.4. Algorithme du tri par sélection

L'algorithme du __tri par sélection__ écrit en pseudo-code est le suivant : 

    fonction tri_selection(tableau T)
        n ← taille de T
        POUR i allant de 1 à n-1 # numérotation des cases de 1 à n
            indice_du_min ← i 
            POUR j allant de i+1 à n
                SI T[j] < T[indice_du_min] ALORS
                    indice_du_min ← j
            FIN_POUR
            échanger T[i] et T[indice_du_min]
        FIN_POUR
        Renvoyer T # Facultatif : en réalité, le tableau T a été modifié sur place.
          
En voici une illustration (où, pour un gain de place, on a noté $k$ la variable `indice_du_min`), avec le même tableau `[12,8,23,10,15]` que précédemment :

![tri-selection_algo_asy_screencopy-2.png](data/tri-selection_algo_asy_screencopy.png){: .center}

### 2.5. Programme Python du tri par sélection

À l'aide de l'algorithme ci-dessus, coder en Python un programme de tri par sélection.

{{ IDE("data/algo_tri_select_vide")}}

??? danger "Solution"
    ```python
    def tri_selection(tab):
        for i in range(len(tab) - 1):
            indice_du_mini = i
            for j in range(i + 1, len(tab)) :
                if tab[j] < tab[indice_du_mini]:
                    indice_du_mini = j
            # on échange alors tab[i] et tab[indice_du_mini]
            temp = tab[i] 
            tab[i] = tab[indice_du_mini]
            tab[indice_du_mini] = temp
        return tab
    ```

## ^^3. Complexité des algorithmes de tri par insertion et par sélection^^

On va constater ***expérimentalement*** que le temps de calcul nécessaire pour trier un tableau par l'un des deux algorithmes de tris ci-dessus augmente, en première approximation, comme le __carré de la taille des tableaux__ à trier. 


!!! success "Expérimentation"
    Copier le code Python ci-dessous dans un IDE puis l'exécuter.

    Cela produit un graphique avec, en abscisse, différentes tailles de tableaux (`[100, 1000, 5000, 10000, 15000, 20000]`) et en ordonnée, le temps de calcul (c'est-à-dire l'écart `end-start`) pour trier chacun des tableaux.

    La courbe doit avoir une forme d'un morceau de **parabole**, ce qui est caractéristique des fonctions de la forme $f(x) = a x^2$ : on visualise bien un lien de proportionnalité avec le ***carré*** de la taille $x$ du tableau. 

    {{IDEv("data/graph_tps_tri")}}
    


!!! tip ""
    On dit que les algorithmes de tri par insertion et de tri par sélection sont de __complexité quadratique__.

    On trouvera parfois l'expression "complexité en $O(n^2)$" pour matérialiser le lien de proportionnalité avec le **carré de la taille** du tableau.

??? done "Vérification mathématique"
    Prenons le cas du tri par sélection : il est composé de **deux boucles FOR imbriquées** l'une dans l'autre.

    Si on faisait un schéma de suivi de l'évolution des variables, on aurait :
    
    - en colonne, la variable `i` qui va de 1 à N (où N est la taille du tableau) ;
    - en ligne, la variable `j` qui va de `i` à N.

    ![](data/complex_quadratique_triangle.png)

    Le schéma de suivi a ainsi une allure de triangle, dont chaque cellule correspond à une étape de calcul.

    Le nombre total d'étapes de calcul correspond alors à l'aire du triangle. Or la formule de l'aire d'un triangle est $\textrm{Aire} = \frac{\textrm{base} \times \textrm{hauteur}}{2}$. 
    
    Ici, avec une base et une hauteur égales à N, on a donc un temps total de calcul égal à $\frac{1}{2} N^2$, ce qui est bien **proportionnel au carré** de la taille N du tableau.


## ^^4 - QCM^^ 

#### Question 1 :

Au cours d'un tri de tableau, on observe les étapes suivantes : 
    
![bns_2.jpg](data/bns_2.jpg){: .center}



{{ multi_qcm(
    [
        "Quel est l'algorithme de tri qui a été utilisé ?",
        ["tri par sélection", "tri par installation", "tri par insertion", "tri par détection"],[3],
    ],
    )}}

??? danger "Explication"
    Dans un tri par insertion, les cases les plus à droite du tableaux ne sont modifiées qu'à la fin de l'algorithme. 
    Mais dans un tri par sélection, elles peuvent se retrouver modifiées dès le début de l'algorithme (cas où la valeur minimum s'y trouve). 

#### Question 2 :

Un algorithme de tri d'une liste d'entiers est implémenté de la façon suivante : 

```python
def trier(L) :
    for i in range(len(L)):
        k = i
        for j in range(i+1, len(L)):
            if L[j] < L[k] :
                k = j
        L[i], L[k] = L[k], L[i]
    return L
```


{{ multi_qcm(
    [
        "Quelle est l'affirmation exacte ?",
        ["cet algorithme est celui du tri par sélection et il a un coût linéaire en la taille de la liste à trier", "cet algorithme est celui du tri par insertion et il a un coût linéaire en la taille de la liste à trier", "cet algorithme est celui du tri par sélection et il a un coût quadratique en la taille de la liste à trier", "cet algorithme est celui du tri par insertion et il a un coût quadratique en la taille de la liste à trier"],[3],
    ],
    shuffle = False)}}

??? danger "Explication"
    Il faut reconnaitre ici l'algorithme de tri par sélection. À noter qu'ici, l'interversion des valeurs `L[i]` et `L[k]` est faite en une seule ligne (_particularité de Python_) alors que dans la version donnée en correction du cours, on utilise une variable temporaire `temp` pour réaliser cette interversion. 

#### Question 3 

Soit T le temps nécessaire pour trier, à l'aide de l'algorithme du tri par insertion, une liste de 1000 nombres entiers.

{{ multi_qcm(
    [
        "Quel est l'ordre de grandeur du temps nécessaire, avec le même algorithme, pour trier une liste de 10 000 entiers, c'est-à-dire une liste dix fois plus grande ?",
        ["à peu près le même temps T", "environ 10 × T", "environ 100 × T", "environ T^2^"],[3],
    ],
    )}}

??? danger "Explication"
    Le temps de tri par insertion d'un tableau de nombres est proportionnel au **carré** de la taille du tableau. Or, si la taille du tableau est multipliée par 10, alors le carré de la taille est multiplié par 10^2^ = 100. Alors le temps de tri passe de T à 100 × T si on multiplie la taille du tableau par 10.

