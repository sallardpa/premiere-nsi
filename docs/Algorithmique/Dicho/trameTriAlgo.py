def recherche_dichotomique(tab, val):
    """
    Entrées : 
        tab est un tableau de nombres trié par ordre croissant 
        val est un nombre
    Sortie : un booléen qui vaut True si val appartient au tableau et False sinon
    """
    # compléter le code