from random import randint
from time import perf_counter

for taille in [10**4, 10**6]:
    tab = [randint(1, taille) for k in range(taille)]
    val_alea = randint(1, taille)
    
    debut = perf_counter()
    recherche_sequentielle(tab, val_alea)
    fin = perf_counter()
    
    print("Pour un tableau de taille n =", taille, "\n il faut ", fin - debut, "secondes pour déterminer si val_alea est dans le tableau")