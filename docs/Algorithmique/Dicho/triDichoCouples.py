def recherche_dichotomique_couple(tab,val):
    """
    Entrées : 
        tab est un tableau de couples (nombre, texte), trié par ordre croissant des nombres
        val est un nombre
    Sortie : un booléen qui vaut True si val est un nombre d'un couple du tableau et False sinon
    """
    # compléter le code

# test de la fonction
tab_departs = [(12.39, 'Londres'), (12.57,'Zurich'), (13.08,'Dublin'), (13.21,'Casablanca'), 
               (13.37,'Amsterdam'),(13.48,'Madrid'),(14.19,'Berlin'), (14.35,'New York'),
              (14.54, 'Rome'), (15.10,'Stockholm')]
assert recherche_dichotomique_couple(tab_departs,14.00) == False