from random import randint
nb_mystere = randint(0,100)

print("Esssayez de trouver le nombre mystère")
nb_essais =0
trouvé = False

while not(trouvé):
    choix = int(input("Entrer un nombre entre 0 et 100 :"))
    nb_essais += 1
    if choix > nb_mystere :
        print("C'est trop grand")
    elif choix < nb_mystere :
        print("C'est trop petit")
    else :
        trouvé = True

print("Bravo ! Nombre de tentatives = ", nb_essais)