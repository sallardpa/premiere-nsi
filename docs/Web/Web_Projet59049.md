# Projet : le jeu de 59049

Le projet consiste à détourner le célèbre jeu de 2048 afin de jouer, non pas avec des puissances de deux (2 → 4 → 8 → 16 → 32 → etc.) mais avec des **puissances de trois** : 3 → 9 → 27 → 81 → 243 → ...

| Jeu original | Jeu modifié | 
| ----------- | ----------- |
|![](data/2048.png) | ![](data/59049.png) | 

Pour cela, un [dossier zippé](data/CodeSource2048.zip) vous est fourni : ce dossier contient le code du jeu de 2048 en HTML / CSS / JavaScript.

Dans le détail, les objectifs du projet sont :

- jouer avec des puissances de 3 : 3 → 9 → 27 → 81 → 243 → ... ;
- conserver les évolutions de couleur des cases ;
- adapter la taille des caractères (car les nombres sont plus grands et prennent plus de place !) ;
- mettre tout l'affichage en français ;
- afficher du message "Gagné" quand on atteint 59049=3^10^ ( au lieu de 2048 = 2^11^).