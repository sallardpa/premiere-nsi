# TP : création d'un site Web

L'objectif du TP est de publier vos pages HTML (ainsi que le fichier de style CSS et le code JavaScript) sur un site Web créé pour l'occasion.

## 1. Prérequis

* Télécharger et installer [Git](https://git-scm.com/download){target="_blank"} : 

    - prendre la version _Standalone Installer_ si vous avez les droits Administrateur sur votre ordinateur ;
    - sinon, prendre la version _Portable_.
    - si vous êtes sous Linux, il n'y a rien à télécharger car Git est déjà installé :wink:

* Après l'installation de ***Git*** :

=== "Git version Standalone"   
    Dans le menu des applications de Windows, rechercher `Git`, ouvrir ***Git Bash*** puis saisir :
 

=== "Git version Portable"

    Aller dans le dossier où vous avez installé `GitPortable`, lancer ***git-bash.exe*** puis saisir :


```console
git config --global user.name "Votre Nom"
git config --global user.email "VotreEmail@mail.fr"
```

## 2. Création d'un dépot Git

Aller sur [GitHub](https://github.com/){target="_blank"} puis :

* renseigner une adresse mail valide dans le champ de saisie puis cliquer sur `Sign up for GitHub` pour vous créer un compte.
* suivre ensuite les consignes de création de compte, :warning: en veillant à choisir un _username_ pertinent car votre site Web sera ensuite accessible à l'adresse `https://VotreUsername.github.io` :warning:.


## 3. Création d'un dépot Git et du site Web statique

!!! done ""
    Ces instructions sont basées sur le mode d'emploi fourni [ici par GitHub](https://pages.github.com/){target="_blank"}.

* Dans votre compte [GitHub](https://github.com/new){target="_blank"} : 

    - selectionner `Your repository` dans le menu en haut à gauche sous votre avatar et cliquer sur `New` pour créer un nouveau dépot.
    - dans le champ de nom de dépot, saisir `VotreUsername.github.io` (en remplaçant évidemment `VotreUsername` par votre propre _username_).

* Sur votre PC :

    - dans le répertoire `C:\Utilisateurs\VotreLogin`, créer un répertoire `MonSiteWeb`.
    - rechercher l'application `Invite de commande` pour ouvrir un terminal, ou bien lancer `git-bash.exe` si vous avez la version _Portable_ de `git`, puis aller dans ce nouveau répertoire à coups de commande `cd`.
    - toujours dans le terminal, saisir la commande `git clone https://github.com/VotreUsername/VotreUsername.github.io` (en remplaçant évidemment `VotreUsername` par votre propre _username_) et suivre les instructions de connexion.
    - dans le répertoire `MonSiteWeb`, un sous-répertoire `VotreUsername.github.io` est apparu : il faut alors y copier tous les fichiers (HTML, CSS, JavaScript, images, etc) de votre projet Web, :warning: en veillant à ce que la page principale HTML soit nommée `index.html`:warning: .
    - reprendre le terminal, se déplacer dans ce sous-répertoire avec la commande `cd VotreUsername.github.io` puis saisir successivement :

    ```console
    > git add --all
    
    > git commit -m "Premier envoi"

    > git push -u origin main
    ```
    - après la dernière commande, une demande de sélection d'authentification apparaît : 

=== "Git version Standalone"   
    Choisir `Device code` puis, sur la page [github.com/login/device](https://github.com/login/device){target="_blank"}, renseigner le code qui est apparu dans le terminal.
 

=== "Git version Portable"
    Choisir `manager-ui`.

* Voilà c'est fini ! Attendre une minute ou deux puis, dans un navigateur Internet, saisir l'adresse  `https://VotreUsername.github.io` pour visualiser votre site.
Il ne vous reste plus qu'à communiquer cette adresse à qui vous le souhaitez. 

* Quand vous mettez à jour vos fichiers HTML, CSS ou Javascript dans le répertoire `MonSiteWeb`, il faut ensuite _"pousser"_ ces modifications sur le dépot GitHub. Pour cela, il faut répéter la saisie des trois commandes ci-dessus `git add... git commit ... git push`.

    

## 4. Pour aller plus loin et travailler avec des outils de développeurs

* Dans le menu des applications de Windows, rechercher Git, ouvrir Git Bash puis saisir la commande`ssh-keygen -t ed25519 -C "Nom_et_Machine"` (peu importe ce qu'il y a dans les guillemets, c'est juste pour y retrouver si vous travaillez sur plusieurs appareils).

Ceci crée un fichier `id_ed25519.pub` dans le répertoire `C:\Utilisateurs\VotreLogin\.ssh`, dont il faut copier le contenu dans l'onglet `Settings\SSH and GPG keys` de votre compte Github.

* Installer [Visual Studio Code](https://code.visualstudio.com/) : 

    - créer, éditer et modifier vos fichiers HTML, CSS ou Javascript.
    - ces  modifications peuvent être "poussées" sur votre dépot Git directement depuis VS Code selon ce [mode d'emploi](https://learn.microsoft.com/fr-fr/training/modules/use-git-from-vs-code/5-exercise-stage-commit).


