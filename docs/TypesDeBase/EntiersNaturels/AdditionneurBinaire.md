# TP : additionneur binaire

## ^^1. Rappels sur les booléens, les transistors et les portes logiques^^

Un transistor est comme une brique élémentaire d'un circuit électronique. De façon simplifiée, il fonctionne comme un interrrupteur :

- il a deux pattes d'entrée, appelées Base (B) et Collecteur (C) et une patte de sortie appelée Emetteur (E)
![](data/transistor.webp){ width="20%" .center}

- la patte  d'entrée B est comme la commande de l'interrupteur :

    - si la patte B n'est pas alimentée (courant = 0), alors la sortie E est toujours à 0 
    - si la patte B est alimentée (courant = 1), alors elle laisse passer ce qui vient de C vers la sortie E

On se rend compte que ce fonctionnement correspond à l'opérateur booléen `ET` : en effet, le booléen `B et C` a la même valeur que `E`.  On parle alors de _porte logique ET_.

<iframe style="width: 100%; height: 220px; border: 0" src="https://logic.modulo-info.ch/?mode=tryout&data=N4IgbiBcAsA0IEsB2UDaoAOB7AzmgjPgAywDsRAuvAgCZQngCGANvQL6ya4HGz4CslanUgAmeGBbsqILAFcALmi55IqAMzqShIYhH42MgOaMFAU1XoQCgJ4YzUEAEEAcgBEQ8bJdG8dM5DR1WGgZeSVIfkN4ZkYAIzNmSxUg8j5iGXMADwiQAFEQDhS1ADYSckyzHMcAIULOEG80Mr5BSurIEABhQpkAdwQAJws0VBJ1KlRxUNhUfj4KCjYgA"></iframe>

En associant plusieurs transistors par des montage en série ou en parallèle, on fabrique d'autres portes logiques : _OU_ (:flag_gb:  OR) et _OU eXclusif_ (:flag_gb: XOR) par exemple. 

!!! done "" 
    Connectez les deux entrées A et B à l'une de ces portes puis reliez la sortie de la porte à S.

<iframe style="width: 100%; height: 220px; border: 0" src="https://logic.modulo-info.ch/?showonly=or,xor&mode=design&data=N4IgbiBcAsA0IEsB2UDaoAOB7AzmgjPgAywDsRAuvAgCZQngCGANvQL6ya4HGz4CslanUgAmeGBbsqILAFcALmi55IqAMzqShIYhH42M5owBGAU2ar0IbFfXk+xGQrMAPJZBABlEBxVoANhJyZzcPEABBX04bbjUgvkFQ9ygQACFfCjYgA"></iframe>


## ^^2. Additionneur binaire^^

* Commençons par le plus simple : l'addition sur 1 bit.

On veut additionner deux nombres binaires A~0~ et B~0~ codés sur 1 seul bit : ces deux nombres ne peuvent valoir que 0 ou 1 et la somme S = A~0~ + B~0~ vaut au maximum (10)~2~. Il faut donc prévoir deux bits de sortie S~1~ et S~0~.

<iframe style="width: 100%; height: 340px; border: 0" src="https://logic.modulo-info.ch/?showonly=and,or,xor&?mode=design&data=N4IgbiBcAsA0IEsB2UDaoAOB7AzmgTPgAyzREC68WATggKZIAuUIe8CAJlCeAIYA23AL6xMuNAEYAzCQCcFKrQbNIrEOy6R88MAOGUQWAK4r0IbHkip8EkvgAcCw0qYs2iTRJFjLqCWVgHJxp6V1V3TigANiEDAHNeRjpfUEYATww6FgANAHkAJXVzcSsbEgl5AxDlNyLkNAB2WHsqkyhZbxB0zJYAQQA5ABEii0kAiuCXFTV2FCtbWAkJVpUJfFj4fl4AIzp+FOLfMtgpYgMkgA9pgGUiEE7R0uJFp0vp3ruHkr8ZWCjXuhXFgAIU+okOYxIpwBQNU1wk9wMAHcENRkmhUCQGpRrM0ceUFLiljjZIscWs-uRyEIgA"></iframe>



!!! tip "Explication"
    Quand on pose l'addition  A~0~ + B~0~ et qu'on écrit le résultat en binaire sous la forme S~1~S~0~, on obtient :
    
    A0 | B0 | S1 | S0
    :---: | :---: | :---: | :---:
    0 | 0 | 0 | 0
    0 | 1 | 0 | 1
    1 | 0 | 0 | 1
    1 | 1 | 1 | 0

    On s'aperçoit alors que :
    
    - S~0~ vaut A~0~ `OU eXclusif` B~0~
    - S~1~ vaut A~0~ `ET` B~0~

* Continuons avec une addition sur 2 bits

Maintenant, les deux nombres A et B sont des nombres binaires à deux bits, que l'on note respectivement sous la forme A~1~A~0~ et B~1~B~0~. La somme S = A+B vaut au maximum (11)~2~ + (11)~2~ = (110)~2~ donc il faut prévoir 3 bits pour la sortie S : on la note S~2~S~1~S~0~.

Puisqu'il faut tenir compte de l'éventuelle retenue dans l'addition des deux premiers bits A~0~ et B~0~, le schéma se complique un peu :


<iframe style="width: 100%; height: 530px; border: 0" src="https://logic.modulo-info.ch/?showonly=and,or,xor&?mode=design&data=N4IgbiBcAsA0IBsCGAjApggzlA2qALgJ4AOaUIATmgMb4jzED22kOAzAEwAMsHAHFwC68AO5RuPEAAsobLpOqMEjCuUIZlY+JnwVGAazQB1AJYATfDMgcAvrFBMWOAKzdYzgIxD4+NAA86SBAAZS4QOwdmXGc5Xm8QXwDyAEEwiJBHXGhnHgA2eMTAkAAhNPsMqNZoeXcvYQT-IuCPcPLM1jYcuPrClJb09pwOLvyexvJi-rbKoYBOHk8C8aDgjlbIpw8u-iWkoKQzMxN8E0YAOzO0AFdVG3qTM9wN6Nic+pUTNDOi7HhzKEkYCQCABAxm2R483eFE+33IvxA-2s8CBIMgXDBTk6C3iHy+P3oiLMUA8bBRwNB0ycw0huJh+PhhKRHjg4Ap6Lu8EYV0CeAqTlcPGgAHY6bCCX9iZAPJisjURWKGUEEUjcrLWBx5rAFdDxYzJSTnJyQABzJC+JwEEhkIIADQA8gAlQmDQVxRVw5VMx6sYWwPjvHlQWbpIikFIAOQAIi7wfl3bqlSAVT6cF5YB4PIHAh5bOUwzaQA7nQwZmxqgmufTPcnvbgPLkM8LsyS+KHreQnbHqVrqh6JYjUxwOLw2C3rNB2+Ggslo93cBx4xJEzWUwvnLxcuOOMKp4XZzHS1iapx+-rBwu+LxZuO5HvyMX5x0uqeVwOHrg2B5YJxb2xjSIJhUE4OA8M2sBDP6wigRm3iQZm0GzBm0GkhmW4QSyTYoWSwwoXAi4oSOX6EbwAYYVev4QWwZLoUMfocGO5HXtBsTDtBHBwFsgiCDYQA"></iframe>



Le bloc encadré, composé d'un assemblage de portes `ET`, `OU` et `OU eXclusif` s'appelle un additionneur. On simplifiera désormais les schémas en utilisant un bloc "additionneur". Par exemple, le schéma ci-dessus est simplifié sous la forme suivante :

<iframe style="width: 100%; height: 350px; border: 0" src="https://logic.modulo-info.ch/?showonly=and,or,xor,adder&?mode=design&data=N4IgbiBcAsA0IEsB2UDaoAOB7AzmgrAMwAMs+xAuvFgE4ICmSALlCHvAgCZSngCGAGx4BfWJlxpo5WAE5K1OoxaQ2IDt0gAmeGEEixIbHkipCJMvJC0GzVu0QaAjIR17IxUeOOpNc2ZeslOzUHKEc4fiF3YSorAFdldEMJE3xNUl8AxVsVey4wz2TvQkcMuVjAnNV1KEJoQqM0TXTYTIrs5WrQyEJ8GPgAcz4mem9QJgBPDHpWAA0AeQAlEMbUlvSsm068lBMAdlgADgqEqBlCyenWAEEAOQARFZTUaAA2DOJNoNyQ5DRS2CORwnZSOTT9EAAYywAFtsEglGMQJcZio+JxOPQaE9igDNKVYn8TIRXrBCAdCMdqKdiTJYNBKBCBHwAEb0ARI1aoNKkMyWEYAD06AGViCAGs8iB9YoLOtcxRLvFJSO8ZfQhawAEIKgxcvmtfnquWOcW656ZWCq+CyrUmxVoEq8khqjUqYV2s3eZpOw2ukDCzTi2IAdwQNFGaFQpD2VB8R1jpAJsDjQNjdOByecZJjmbgJNjYLJVNMdLqsYZZPwFAowiAA"></iframe>

!!! faq "Exercice : réaliser une addition sur 4 bits"
    Utiliser les composants proposés pour pouvoir effectuer l'addition de deux nombres A = A~3~A~2~A~1~A~0~  et B= B~3~B~2~B~1~B~0~ codés sur 4 bits. 

    🧭 Indication : la sortie `Cout`, qui correspond à l'éventuelle retenue, se branche sur l'entrée `Cin` de l'additionneur suivant.


<iframe style="width: 100%; height: 360px; border: 0" src="https://logic.modulo-info.ch/?showonly=and,or,xor,adder&?mode=design&data=N4IgbiBcAsA0IEsB2UDaoAOB7Azmg7AIwAMsArMQLrxYBOCApkgC5Qh7wIAmUp4AhgBteAX1iZcaAGzRSADio16TVpHYhOPSACZ4YIaPEhseSKjIBmUhWog6jFmw6IthC3oORiYiadTR8eUU7ZUc1Z24oQjgBYS8fY0kzCylrYPsVJw0XKGgpDzjvIxM0QkDydNDVdU1c-ALDXzQLbSDbDLCanMh8d1jGxL83NqUHaoitfBj9QpF2gFdVdEGCYlIrSrGs2shCBJKzMhJYDfaq7e6LaH2k1Cv1tbOt8OzIyAsyG6GKE8fRzJeOwoXzQP1O-06EygRzm8EE-AARgxBH4mmZAutZLZmAwAB7VADKxBAIPRx1a2Lx1QAgsTSagZKQsfAcfi2AAhOnFW6WUgUllUtjUwgk7l+AJM4Ks6rskX0o6YqWCtQEuVi5qyE7MkDStgE7SitGoQhg7W6lUWQ0rQ6KylslXQK0HO6pWD8nXKkDUg30lppO0yn3qsxlPlK+1ey304YVAMcqOUERAA"></iframe>