# Représentation des données : les types de base et leur codage :butterfly:

Dans cette partie, sont traités les thèmes :

* les valeurs booléennes ☯️
* codage des nombres entiers naturels (_entiers positifs_) :family_mwgb:
* codage des nombres entiers relatifs (_tous les entiers, positifs et négatifs_) :night_with_stars:
* codage des nombres réels :sailboat:
* codage des caractères d'un texte :book:
