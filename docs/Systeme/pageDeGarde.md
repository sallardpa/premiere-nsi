# Architectures matérielles et systèmes d'exploitation :floppy_disk:

Dans cette partie, sont traités les thèmes :

* histoire et architecture des ordinateurs
* systèmes d'exploitation
* introduction aux commandes Linux
* périphériques entrées/sorties et Interface Homme-Machine : la carte micro:bit 
* réseaux de données