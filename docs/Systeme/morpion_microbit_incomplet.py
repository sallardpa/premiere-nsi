# Imports go at the top
from microbit import *
from random import randint

def microbit_pose_pion() :
    # place un pion aléatoirement sur la grille sur une case libre
    place_libre = False
    while not place_libre :
        x = randint(1,3)
        y = randint(1,3)
        val_pixel = display.get_pixel(x,y) # on récupère l'intensité du pixel
        if ... : # à compléter
            place_libre = ...  # à compléter
            display.set_pixel(x,y,lumi_microbit)
    # après avoir trouvé une place libre, on fait une pause de 1000 millisecondes
    sleep(1000)
    return None

def joueur_pose_pion() :
    numero_led = 5 # pour démarrer sur la 2e ligne à gauche
    position_valide = False
    while not position_valide :
        x = numero_led % 5 # 
        y = numero_led // 5 # 
        val_pixel = display.get_pixel(x,y) # on mémorise l'intensité du pixel
        display.set_pixel(x,y,9) # puis on le met en surbrillance
        if button_a.is_pressed() and button_b.is_pressed() : # les deux boutons pour valider
            if val_pixel == ... and x>=... and x<=... and y>=... and y<=... : # à compléter
                position_valide = ... # à compléter
        if button_a.was_pressed(): # reculer
            numero_led = ... # à compléter
            if numero_led < 0 : 
                numero_led = 0
        if button_b.was_pressed(): # avancer
            numero_led  = ... # à compléter
            if numero_led > 24 :
                numero_led = 24
        display.set_pixel(x,y,val_pixel) # on remet le pixel dans son état initial
    # le joueur a validé la position
    x = numero_led % 5 # 
    y = numero_led // 5 # 
    display.set_pixel(x,y,lumi_joueur)
    return None

def tout_rempli() :
    # renvoie True si toutes les cases sont remplies et False sinon
    for x in range(1,4) :
        for y in range(1,4):
            ...
    # on est sorti des deux boucles imbriquées
    return ...

def test_lignes(numero) :
    # renvoie True s'il y a un alignement de pions sur une des lignes de la grille
    # pour le joueur dont le numéro est passé en entrée de la fonction
    # et False sinon
    for ligne in range(1,4) :
        total = 0
        for x in range(1,4) :
            total += display.get_pixel(x,ligne)
        if total == 3 * tab_lumi[numero] :
            return True
    # on est sorti des deux boucles imbriquées
    return False

def test_colonnes(numero) :
    # renvoie True s'il y a un alignement de pions sur une des colonnes de la grille
    # pour le joueur dont le numéro est passé en entrée de la fonction
    # et False sinon
    ... # à compléter

def test_diagonale(numero) :
    # renvoie True s'il y a un alignement de pions sur la diagonale principale de la grille
    # pour le joueur dont le numéro est passé en entrée de la fonction
    # et False sinon
    ... # à compléter

def test_antidiagonale(numero) :
    # renvoie True s'il y a un alignement de pions sur l'autre diagonale de la grille
    # pour le joueur dont le numéro est passé en entrée de la fonction
    # et False sinon
    ... # à compléter

def test_alignement(numero) :
    # renvoie True s'il y a un alignement de pions pour le joueur dont le numéro est passé en argument
    aligne = test_lignes(numero) or test_colonnes(numero) or test_diagonale(numero) or test_antidiagonale(numero)
    return aligne


def lancer_partie() :
    # initialisation
    num_joueur = randint(0,1) # 0 pour le vrai joueur, 1 pour le microbit
    if num_joueur == 0 :
        display.scroll("A vous")
    else :
        display.scroll("A moi")
    # code principal du jeu
    partie_finie = False
    while ... : # à compléter
        # un joueur pose son pion
        if num_joueur == 0 :
            joueur_pose_pion()
        else :
            microbit_pose_pion()
        # on regarde s'il y a un alignement
        if test_alignement(num_joueur) :
            partie_finie = ... # à compléter
            if num_joueur == 0 :
                display.show(Image.HAPPY)
            else :
                display.show(Image.SAD)
        # sinon on regarde s'il reste des cases libres
        elif tout_rempli() :
            partie_finie = ... # à compléter
            display.scroll("Fini")
        # on passe la main à l'autre joueur
        num_joueur = (num_joueur + 1) %2 # fait passer de 0 à 1, et de 1 à 0
    # la partie est finie, on sort de la boucle While
    return None

# corps du programme

# variables globales, qu'on peut utiliser dans les fonctions
# mais sans pouvoir les modifier dans les fonctions
lumi_joueur = 7
lumi_microbit = 3
tab_lumi = [lumi_joueur, lumi_microbit]

while True :
    if accelerometer.was_gesture('shake'):
        lancer_partie()
