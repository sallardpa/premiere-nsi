# Microbit 🌡️


## ^^0. Présentation de la carte BBC micro:bit^^

**BBC micro:bit** est une carte à [microcontrôleur](https://fr.wikipedia.org/wiki/Microcontr%C3%B4leur){target="_blank"} conçue en 2015 au Royaume-Uni pour développer l'apprentissage de l'algorithmique et de la programmation.

![](images/microbit1.png){: .center}


La carte micro:bit dispose des [spécificités techniques](https://microbit.org/fr/get-started/user-guide/overview/){target="_blank"}  suivantes :

- 25 LEDs programmables individuellement
- 2 boutons programmables
- Microphone et haut-parleur
- Capteurs de lumière, de température et de mouvements (accéléromètre et boussole)
- Communication sans fil, via Radio et Bluetooth
- Interface USB et broches de connexion

## ^^1. Premiers pas^^

### 1.1.  Écrire un premier script

Rendez-vous sur la page [https://python.microbit.org/](https://python.microbit.org/v/3){target="_blank"}

Le site vous propose de tester le code suivant :

```python
from microbit import *

while True:
    display.show(Image.HEART)
    sleep(1000)
    display.scroll('Hello')
```

Dans le simulateur en haut à droite, cliquez sur le triangle bleu. C'est parti !


### 1.2 Avec une micro:bit réelle

1. Branchez la carte sur un port USB. Un lecteur MICROBIT apparaît dans les périphériques USB.
2. Écrire un script sur l'éditeur en ligne [https://python.microbit.org/](https://python.microbit.org/v/3){target="_blank"}
3. Cliquer sur le bouton __Save__ pour télécharger et sauvegarder un fichier __.hex__
4. Copier-coller ce fichier .hex dans votre lecteur micro:bit.

Cette procédure est à répéter à chaque nouveau code.

## ^^2. Découverte des fonctionnalités^^


###  2.1. Commandes de base de l'afficheur, matrice de 5x5 LEDs


LED signifie _Light Emitting Diode_, qui se traduit par "Diode électroluminescente". 
La carte micro:bit en dispose de 25, toutes programmables individuellement, ce qui permet d'afficher du texte, des nombres et des images.


On peut afficher :

* du texte, soit lettre par lettre avec la commande `display.show(monTexte)`, soit en défilant avec la commande `display.scroll(monTexte)` :

```python
from microbit import *
display.show('NSI')
sleep(500)
display.scroll('NSI')
```

* des images préprogrammées, avec encore la commande `display.show(nom_de_Image)`

```python
from microbit import *
display.show(Image.SAD)
```

??? note "Liste des images disponibles"
    ```
    Image.HEART
    Image.HEART_SMALL
    Image.HAPPY
    Image.SMILE
    Image.SAD
    Image.CONFUSED
    Image.ANGRY
    Image.ASLEEP
    Image.SURPRISED
    Image.SILLY
    Image.FABULOUS
    Image.MEH
    Image.YES
    Image.NO
    Image.CLOCK12
    Image.CLOCK11
    Image.CLOCK10
    Image.CLOCK9
    Image.CLOCK8
    Image.CLOCK7
    Image.CLOCK6
    Image.CLOCK5
    Image.CLOCK4
    Image.CLOCK3
    Image.CLOCK2
    Image.CLOCK1
    Image.ARROW_N
    Image.ARROW_NE
    Image.ARROW_E
    Image.ARROW_SE
    Image.ARROW_S
    Image.ARROW_SW
    Image.ARROW_W
    Image.ARROW_NW
    Image.TRIANGLE
    Image.TRIANGLE_LEFT
    Image.CHESSBOARD
    Image.DIAMOND
    Image.DIAMOND_SMALL
    Image.SQUARE
    Image.SQUARE_SMALL
    Image.RABBIT
    Image.COW
    Image.MUSIC_CROTCHET
    Image.MUSIC_QUAVER
    Image.MUSIC_QUAVERS
    Image.PITCHFORK
    Image.XMAS
    Image.PACMAN
    Image.TARGET
    Image.TSHIRT
    Image.ROLLERSKATE
    Image.DUCK
    Image.HOUSE
    Image.TORTOISE
    Image.BUTTERFLY
    Image.STICKFIGURE
    Image.GHOST
    Image.SWORD
    Image.GIRAFFE
    Image.SKULL
    Image.UMBRELLA
    Image.SNAKE
    ``` 


* des images personnalisées, avec encore la commande `display.show(nom_de_Image)`, en codant l'intensité lumineuse de chacune des 25 LEDs comme dans l'exemple ci-dessous. Chaque nombre représente l'intensité lumineuse d'une LED, sachant que 0 correspond à "intensité lumineuse nulle" et que 9 correspond à "intensité lumineuse maximale".


```python
from microbit import *

monBateau = Image("05050:"
               "05050:"
               "05050:"
               "99999:"
               "09990")

display.show(monBateau)
```


* des pixels individuels, avec la commande `display.set_pixel(x, y, val)` pù

    -  `x` et `y` sont les coordonnées du pixel, forcément comprises entre 0 et 4. 

![](images/led_coord.png){: .center}

    - `val` est la luminosité des pixels, qui est un nombre compris entre 0 (désactivé) et 9 (luminosité maximale).

```python
from microbit import *
display.set_pixel(1, 4, 9)
```


Dans le programme suivant, on importe `randint` du module `random` de MicroPython et on l'utilise pour afficher un pixel au hasard sur la matrice.


```python
from microbit import *
from random import randint
x=randint(0,4)
y=randint(0,4)
display.set_pixel(x, y, 9)
```

Tester le programme précédent plusieurs fois de suite. Pour cela, redémarrer la micro:bit en appuyant sur le bouton `RESET` situé à l'arrière de la carte.

### 2.2. Boucle `while`
Le programme suivant utilise une boucle `while` pour faire clignoter le pixel central de manière répétée sur l'écran. La boucle `while` se répète tant que la condition spécifiée est vraie (`True`). Avec un `while True`, la condition est toujours vraie donc cela crée une *boucle infinie*. 

Exécuter le programme ci-dessous:


```python
from microbit import *
while True:
    display.set_pixel(2, 2, 9)
    sleep(500)
    display.clear()
    sleep(500)
```

L'instruction de veille `sleep(N)` provoque la pause du micro:bit pendant `N` millisecondes.

L'instruction `display.clear()` éteint l'affichage.

### 2.3. Boucle `for`

Le programme suivant utilise une boucle `for` pour faire défiler un pixel sur une ligne. Exécutez-le.


```python
from microbit import *
while True:
    for x in range(5):
        display.set_pixel(x,0,9)
        sleep(200)
        display.clear()
```

On peut aussi se servir d'une boucle `for` pour parcourir un tableau et afficher son contenu :

```python
from microbit import *
tab = ['3','2','1','Go']
while True:
    for k  in range(len(tab)):
        display.scroll(tab[k])
        sleep(500)
```


### 2.4. Les entrées boutons A et B : la programmation événementielle

![image](images/mb_AB.png){: .center width=30%}


 Il y a deux boutons sur la face avant du micro:bit (étiquetés A et B). On peut détecter quand ces boutons sont pressés, ce qui permet de déclencher des instructions sur l'appareil.

Exemples avec le bouton A:

- `button_a.is_pressed()`: renvoie `True` si le bouton spécifié est actuellement enfoncé et `False` sinon.
- `button_a.was_pressed()`: renvoie `True` ou `False` pour indiquer si le bouton a été appuyé depuis le démarrage de l'appareil ou la dernière fois que cette méthode a été appelée.

**Exemple :** Essayer le programme suivant qui fait défiler le texte "NSI" indéfiniment. Grâce à l'instruction conditionnelle `if` qui va tester si le bouton A a été pressé (pendant le défilement du texte ou pendant la pause), on va pouvoir sortir de la boucle infinie `while True` en exécutant la commande `break`.


```python
from microbit import *
while True:
    display.scroll("NSI")
    sleep(200)
    if button_a.was_pressed():
        break
```

!!! question "Exercice 1"
    === "Énoncé"
        Créer le code permettant de basculer d'un visage triste à un visage heureux suivant qu'on appuie sur A ou sur B. 

        ![](images/exo1.webp){: .center}

    === "Correction"
        ```python
        from microbit import *
        display.clear()
        while True:
            if button_a.was_pressed():
                display.show(Image.SAD)
            if button_b.was_pressed():
                display.show(Image.HAPPY)
        ```


!!! question "Exercice 2"
    === "Énoncé"
        On veut créer le code permettant de déplacer un point vers la gauche ou vers la droite en appuyant sur A ou sur B.

        ![](images/exo2.webp){: .center}

        Compléter le code proposé :


        ```python
        from microbit import *
        display.clear()
        numero_led = 12 # pour démarrer au milieu
        while True:
            x = numero_led % 5 # 
            y = numero_led // 5 # 
            display.set_pixel(x,y,9)
            if button_a.was_pressed():
                display.set_pixel(x,y,0)
                numero_led = ... # à compléter
                if numero_led < 0 : 
                    numero_led = 0
            if button_b.was_pressed():
                display.set_pixel(x,y,0)
                numero_led = ... # à compléter
                if numero_led > 24 :
                    numero_led = 24
        ```

    === "Correction"
        On écrit `numero_led -= 1` dans le premier cas et `numero_led += 1` dans le second cas.


!!! question "Exercice 3"
    === "Énoncé"
        On veut créer un code permettant de faire défiler toutes les images disponibles, en appuyant sur le bouton B pour passer à l'image suivante, et sur le bouton A pour revenir à l'image précédente.

        ![](images/exo3.webp){: .center}

        Compléter le code proposé :


        ```python
        from microbit import *

        tab_Image = [Image.HEART, Image.HEART_SMALL, Image.HAPPY, Image.SMILE,
               Image.SAD, Image.CONFUSED, Image.ANGRY, Image.ASLEEP, Image.SURPRISED, Image.SILLY,
               Image.FABULOUS, Image.MEH, Image.YES, Image.NO, Image.CLOCK12,
               Image.CLOCK11, Image.CLOCK10, Image.CLOCK9, Image.CLOCK8, Image.CLOCK7,
               Image.CLOCK6, Image.CLOCK5, Image.CLOCK4, Image.CLOCK3, Image.CLOCK2,
               Image.CLOCK1, Image.ARROW_N, Image.ARROW_NE, Image.ARROW_E, Image.ARROW_SE,
               Image.ARROW_S, Image.ARROW_SW, Image.ARROW_W, Image.ARROW_NW, Image.TRIANGLE,
               Image.TRIANGLE_LEFT, Image.CHESSBOARD, Image.DIAMOND, Image.DIAMOND_SMALL, Image.SQUARE,
               Image.SQUARE_SMALL, Image.RABBIT, Image.COW, Image.MUSIC_CROTCHET, Image.MUSIC_QUAVER,
               Image.MUSIC_QUAVERS, Image.PITCHFORK, Image.XMAS, Image.PACMAN, Image.TARGET, Image.TSHIRT,
               Image.ROLLERSKATE, Image.DUCK, Image.HOUSE, Image.TORTOISE, Image.BUTTERFLY, Image.STICKFIGURE,
               Image.GHOST, Image.SWORD, Image.GIRAFFE, Image.SKULL, Image.UMBRELLA, Image.SNAKE]

        numero = 9 # pour commencer à la dixième image
        # compléter ici


        ```

    === "Correction"
        On complète avec le code suivant :
        ```python
        while True:
            display.show(tab_Image[numero])
            if button_a.was_pressed():
                numero -= 1
                if numero < 0 : 
                    numero = 0
            if button_b.was_pressed():
                numero += 1
                if numero >= len(tab_Image) :
                    numero = len(tab_Image) -1
        ```



### 2.5. Capteur de lumière

En inversant les LEDs d'un écran pour devenir un point d'entrée, l'écran LED devient un capteur de lumière basique, permettant de détecter la luminosité ambiante.

La commande `display.read_light_level()` retourne un entier compris entre 0 et 255 représentant le niveau de lumière.

**Exercice :** Compléter le programme ci-dessous qui affiche une image de lune si on baisse la luminosité (en recouvrant la carte avec sa main par exemple) et un soleil sinon.


```python

from microbit import *

soleil = Image("90909:"
               "09990:"
               "99999:"
               "09990:"
               "90909:")

lune = Image("00999:"
             "09990:"
             "09900:"
             "09990:"
             "00999:")

while True:
    if display.read_light_level()> ... : #trouver la bonne valeur (entre 0 et 255)
        display.show(soleil)
    else:
        display.show(...) #trouver la bonne variable
    sleep(500)
```

**Prolongement:** 

* créer un programme qui affiche le niveau de luminosité et le tester avec la LED d'un téléphone portable ou une lampe-torche par exemple. 
* créer un programme qui affiche d'autant plus de LEDs sur la matrice que le niveau de luminosité est faible (0 LED allumée si le niveau de luminosité vaut 255 et les 25 LEDs allumées si le niveau de luminosité vaut 0).


### 2.6. Capteur de température

Le micro:bit n'a pas un capteur de température dédié. Au lieu de cela, la température fournie est en fait la température de la puce de silicium du processeur principal. Comme le processeur chauffe peu en fonctionnement (c'est un processeur ARM à grande efficacité), sa température est une bonne approximation de la température ambiante.
L'instruction `temperature()` renvoie la température de la carte micro:bit en degrés Celsius.

**Exercice :** Écrire un programme qui affiche la température.

### 2.7. Accéléromètre

Un accéléromètre mesure l'accélération de la carte micro:bit, ce composant détecte quand la micro:bit est en mouvement. Il peut aussi détecter d'autres actions, par exemple quand elle est secouée, inclinée ou qu'elle tombe.


La carte micro:bit est munie d'un accéléromètre. Il mesure le mouvement selon trois axes :

- X : l'inclinaison de gauche à droite.
- Y : l'inclinaison d'avant en arrière.
- Z : le mouvement haut et bas.

Dans l'exemple suivant à essayer, l'instruction `accelerometer.get_x()` permet de détecter un mouvement de gauche à droite en renvoyant un nombre compris entre -1023 et 1023; 0 étant la position "d'équilibre".


```python
from microbit import *

while True:
    a_droite = accelerometer.get_x()
    if a_droite > 500:
        display.show(Image.ARROW_E)
    elif a_droite < -500:
        display.show(Image.ARROW_W)
    else:
        display.show("-")
```

Dans le second exemple suivant, l'instruction `accelerometer.is_gesture(shake)` teste si la carte est secouée et on s'en sert pour simuler de lancer d'un dé à 6 faces.

```python
from microbit import *
import random

while True:
    if accelerometer.was_gesture('shake'):
        display.show(random.randint(1, 6))
    sleep(500)
```


### 2.8. Boussole

La boussole détecte le champ magnétique de la Terre, nous permettant de savoir quelle direction la micro:bit indique. 

La boussole doit être étalonnée avant de pouvoir être utilisée. Pour cela, on utilise `compass.calibrate()` qui exécute un petit jeu: au départ, micro:bit fait défiler "Tilt to fill screen". Ensuite, incliner micro:bit pour déplacer le point au centre de l'écran autour jusqu'à ce que vous ayez rempli la totalité de l'écran.

La fonction `compass.heading()` donne le cap de la boussole sous la forme d'un entier compris entre 0 et 360, représentant l'angle en degrés, dans le sens des aiguilles d'une montre, avec le nord égal à 0.

!!! question "Exercice"
    Compléter le programme suivant qui indique le Nord, avec une précision de ± 20°. 

    ```python
    from microbit import *
    compass.calibrate()
    while True:
        if compass.heading() < ... or compass.heading() > ... : 
            display.show(Image.ARROW_N)
        else:
            display.show(Image.DIAMOND_SMALL)
    ```

**Prolongement:** Améliorer le programme pour que le micro:bit indique "N", "S", "E" et "O" en fonction de l'orientation de la boussole.


**Autre prolongement:** étudier l'intensité du champ magnétique autour du périphérique (en utilisant la fonction `compass.get_field_strength()`). Plus d'informations sur les fonctions "boussole" [ici](https://microbit-micropython.readthedocs.io/fr/latest/tutorials/direction.html){target="_blank"}.


*document basé sur le travail de Thomas Basso, académie de Polynésie et de [Gilles Lassus](https://glassus.github.io/premiere_nsi/T3_Architecture_materielle/3.1_Microbit/cours/){target="_blank"}.*