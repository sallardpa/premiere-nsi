# TP : jeu du Morpion sur microbit 🌡️

On veut coder le célèbre jeu du morpion sur une carte microbit. Pour gagner, il faut aligner trois pions soit en ligne, soit en colonne, soit en diagonale.

La grille de jeu sera constituée par les 9 LED centrales de la carte microbit. Le joueur, *c'est-à-dire vous*, jouerez contre la carte microbit elle-même.

![](images/microbit_morp1.png){: .center}

Puisqu'on ne peux pas dessiner des croix ou des ronds, on différenciera les pions du joueur et ceux de la carte par la luminosité du pixel : 7 pour le joueur, 3 pour la carte.
![](images/microbit_morp2.png){: .center}


Pour démarrer une partie, il faudra secouer la carte microbit et un tirage aléatoire décidera qui jouera le premier tour : le joueur ou la carte microbit.

La carte microbit jouera "bêtement" : elle placera son pion au hasard parmi les places vides.

Quand ce sera le tour du joueur, un point lumineux s'allumera à gauche de la grille de jeu (_voir la première image_) avec une luminosité maximale (9) : le joueur déplacera ce point brillant avec les boutons A (pour reculer) et B (pour avancer) et il valide la position en pressant sur A et B en même temps.


### 1. Organisation du code

Télécharger le fichier [morpion_microbit_incomplet.py](morpion_microbit_incomplet.py), l'ouvrir avec un éditeur Python (Thonny, Spyder, etc.) ou bien avec le Bloc-Notes puis copier son contenu 
sur l'éditeur en ligne [https://python.microbit.org/](https://python.microbit.org/v/3){target="_blank"}.


Le corps du programme est tout simple :

- création de trois variables `lumi_joueur`, `lumi_microbit` et `tab_lumi` ;
- puis, dans une boucle infinie `while True`, si on détecte un mouvement de la carte, on appelle la fonction `lancer_partie()`.

```python
def lancer_partie() :
    ... # voir plus bas

# corps du programme
lumi_joueur = 7 # variable globale
lumi_microbit = 3 # variable globale
tab_lumi = [lumi_joueur, lumi_microbit] # variable globale

while True :
    if accelerometer.was_gesture('shake'):
        lancer_partie()
```


### 2. La fonction `lancer_partie`

Le rôle de la fonction `lancer_partie()` est de :

- initialiser une partie en tirant au sort le numéro du joueur qui commencera : 0 pour le joueur humain, 1 pour la carte microbit ;
- et gérer les tours de jeu à l'aide d'une boucle `while` dans laquelle, à chaque tour de boucle :

    - il faut faire jouer le joueur dont c'est le tour en appelant soit la fonction `joueur_pose_pion()`, soit la fonction `microbit_pose_pion()` ;
    - puis il faut détecter par un appel de la fonction `test_alignement(numero)` s'il y a un alignement de pions , auquel cas la partie est finie ;
    - puis vérifier par un appel de la fonction `tout_rempli()` s'il y a encore des cases vides car sinon la partie est finie ;
    - et passer la main à l'autre joueur.

Compléter le code de la fonction `lancer_partie()`.


### 3. La fonction `microbit_pose_pion`

Le rôle de cette fonction est de placer un pion pour le compte de la carte microbit : les coordonnées `(x,y)` sont choisies aléatoirement entre 1 et 3. Mais comme on ne peut pas être certain que la case est libre, il faut refaire ce tirage **tant que** on n'est pas tombé sur une place libre.

Et pour savoir si une case est libre, il suffit de regarder sa luminosité du pixel, donnée par la fonction `display.get_pixel(x,y)` : si ça vaut 0, la case est libre et sinon elle est occupée.

Compléter le code de cette fonction `microbit_pose_pion`.

```python
def microbit_pose_pion() :
    # place un pion aléatoirement sur la grille sur une case libre
    place_libre = False
    while not place_libre :
        x = randint(1,3)
        y = randint(1,3)
        val_pixel = display.get_pixel(x,y) # on récupère l'intensité du pixel
        if ... : # à compléter
            place_libre = ...  # à compléter
            display.set_pixel(x,y,lumi_microbit)
    # après avoir trouvé une place libre, on fait une pause de 1000 millisecondes
    sleep(1000)
    return None
```

### 4. La fonction `joueur_pose_pion`

Le code de cette fonction est presque complètement fourni. Il reste toutefois à compléter

-  le code de l'instruction `if ...` correspondant au moment où le joueur presse sur les boutons A et B en même temps pour déposer son pion : la position en question n'est déclarée valide que 

    - si la case est inoccupée
    - et si les coordonnées `(x,y)` correspondent bien à une des 9 LED du centre de la carte.

- et à modifier la valeur de la variable `numero_led` dans les deux blocs `if` suivants.


Compléter ce code.

### 5. La fonction `tout_rempli`

La fonction `tout_rempli()` doit renvoyer `True` si toute la grille est remplie et `False` sinon.

Compléter son code :

```python
def tout_rempli() :
    # renvoie True si toutes les cases sont remplies et False sinon
    for x in range(1,4) :
        for y in range(1,4):
            ...
    # on est sorti des deux boucles imbriquées
    return ...
```

### 6. Les tests d'alignements

La fonction `test_alignement(numero)`, qui doit renvoyer `True` s'il y a un alignement de pions pour le joueur dont le numéro est passé en argument de la fonction, combine avec des `or` les résultats de quatre tests d'alignements :

- alignement en ligne, donné par l'appel de la fonction `test_lignes(numero)` 
- alignement en colonne, donné par l'appel de la fonction `test_colonnes(numero)` 
- alignement sur une diagonale ou sur l'autre, donné par l'appel des fonctions `test_diagonale(numero)` et `test_antidiagonale(numero)`.

Le code de la fonction `test_lignes(numero)` vous est fourni. Il doit vous servir de modèle pour compléter les codes des trois autres fonctions.


### 7. Et jouez maintenant :satisfied: