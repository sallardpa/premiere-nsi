
## TP sur les dictionnaires



## Exercice 1 : le jeu de Scrabble

Le Scrabble est un jeu de société où l'on doit former des mots avec un tirage aléatoire des lettres.

Chaque lettre vaut un certain nombre de points : 

* les 10 lettres courantes A, E, I, L, N, O, R, S, T et U valent 1 point ;
* D, G et M valent 2 points ;
* B, C et P valent 3 points ;
* F, H et V valent 4 points ;
* J et Q valent 8 points ;
* K, W, X, Y et Z valent 10 points.

Le but du jeu est de former des mots valant un maximum de points quand on additionne les lettres du mot.

Par exemple, le mot 'NSI' vaut 1+1+1 = 3 points, le mot 'VOLTAIRE' vaut 4 + 7×1 = 11 points et 'ZYGOMATIQUE' vaut 2×10 + 8 + 2×2 + 6×1 = 38 points.


!!! question "Question 1"
    Définir un dictionnaire `scrabble` dont les clés sont les 26 lettres de l'alphabet et les valeurs sont les points de ces lettres au Scrabble.

    {{ IDE() }}

!!! question "Question 2"
    Écrire une fonction `valeur_mot` qui prend comme paramètre une chaine de caractères `mot` et qui renvoie la valeur de ce mot. Par exemple, `valeur_mot("VOLTAIRE")` doit renvoyer 11.

    _Indication : il s'agit de parcourir tous les caractères de `mot` et d'additionner les valeurs correspondant à ces clés du dictionnaire `scrabble`_.

    {{ IDE('script_exo7') }}


## Exercice 2 : comptage des lettres d'un mot

On veut écrire une fonction `comptage_lettres` qui prend comme paramètre une chaine de caractères `mot` et qui renvoie un dictionnaire dans lequel :

* les clés sont les différentes lettres de `mot`
* et la valeur associée à chaque clé est l'effectif de cette clé.

Par exemple, l'appel `comptage_lettres("abracadabra")` doit renvoyer le dictionnaire `{'a': 5, 'b': 2, 'r': 2, 'c': 1, 'd':1}`

Compléter le code de cette fonction.

{{ IDE('scriptDicoEffectif') }}


## Exercice 3 : inversion d'un dictionnaire

Dans cet exercice, on appelle "dictionnaire de langues" un dictionnaire dont les clés sont des mots et les valeurs sont les traductions de ces clés dans une autre langue.

Par exemple, `{"chien": "dog", "vache": "cow", "cochon": "pig", "canard": "duck"}` est un dictionnaire de langues. 

Et le dictionnaire inversé d'un dictionnaire de langues est le dictionnaire qui donne la traduction dans l'autre sens : par exemple, le dictionnaire inversé du dictionnaire précédent est 
 `{"dog": "chien", "cow":  "vache", "pig": "cochon", "duck": "canard"}`.

On veut écrire une fonction `inversion` qui prend en paramètre un dictionnaire de langues et qui renvoie le dictionnaire inversé.

**1.**  

{{ multi_qcm(
    [
        "Quelle type de boucle envisagez-vous pour le code de cette fonction ?",
        ["for cle in dico.keys()","for valeur in dico.values()", "for (cle, valeur) in dico.items()", " je n'utiliserai aucune boucle"],[3],
    ],
    shuffle = False)}}

**2.** Proposer un code pour cette fonction.

{{ IDE('scriptDicoInversion') }}

## Exercice 4 :  les Pokemons


On modélise des informations (nom, taille et poids) sur des pokemons de la façon suivante :


{{ IDEv('script_exo1', MAX = 5)}}


Par exemple, Bulbizarre est un pokémon qui mesure 2 m et qui pèse 10 kg.


**1.** 

{{ multi_qcm(
    [
        "Quel est le type de `#!py exemple_pokemons` ?",
        ["c'est un tableau (type `#!py list`)","c'est un p-uplet (type `#!py tuple`)", "c'est un dictionnaire (type `#!py dict`)"],[3],
    ],
    shuffle = False)}}


**2.** Ajouter à cette structure de données le pokémon Goupix qui mesure 4 m et qui pèse 10 kg ?


{{ terminal() }}


**3.** Compléter le code de la fonction `taille(dico, nom)` qui prend en paramètre un dictionnaire de pokémons ainsi que le nom d'un pokemon, et qui renvoie la taille de ce pokemon.

{{ IDE('script_exo4')}}

**4.** On donne le code suivant :

{{ IDE('script_exo2')}}


> __Remarque :__ `None` permet d'introduire une variable sans lui donner de valeur au départ ; c'est une manière de dire "vide" ou bien "rien".

Quelle valeur est renvoyée par l'appel `mystere(exemple_pokemons)` ? En déduire le rôle de cette fonction.

**5.** Compléter le code de la fonction `le_plus_leger(dico)` qui prend des pokemons en paramètre et qui renvoie un tuple dont la première composante est le nom du pokemon le plus léger et la deuxième composante est son poids.

{{ IDE('script_exo3')}}



## Exercice 5 : Au zoo


Au zoo de Beauval, il y a 5 éléphants d'Afrique, 17 écureuils d'Asie, 2 pandas d'Asie ... etc.

On le représente à l'aide d'un dictionnaire, de la façon suivante :

{{ IDEv('script_exo5') }}

On représente de la même façon le zoo de La Flèche :

{{ IDEv('script_exo6') }}


__1.__ On souhaite se doter d'une fonction `plus_grand_nombre(zoo)` qui prend un zoo en paramètre et qui renvoie le nom de l'animal que l'on trouve en plus grand nombre dans ce zoo.

Par exemple, on veut avoir le fonctionnement suivant :
```pycon
>>> plus_grand_nombre(zoo_beauval)
'écureuil'
>>> plus_grand_nombre(zoo_la_fleche)
'girafe'
```


!!! question ""

    {{ multi_qcm(
        [
            "Quelle type de boucle envisagez-vous pour le code de cette fonction ?",
            ["for cle in dico.keys()","for valeur in dico.values()", "for (cle, valeur) in dico.items()", " je n'utiliserai aucune boucle"],[3],
        ],
        shuffle = False)}}


!!! question "Écrire le code de cette fonction"

    {{ IDE() }}


__2.__  On souhaite se doter d'une fonction `nombre_total(zoo, continent)` qui prend un zoo en paramètre ainsi que le nom d'un continent, et qui renvoie le nombre d'animaux originaires de ce continent dans le zoo.

Par exemple, on veut avoir le fonctionnement suivant :

```pycon
>>> nombre_total(zoo_beauval, 'Asie')
19
>>> nombre_total(zoo_la_fleche, 'Afrique')
14
```

!!! question ""

    {{ multi_qcm(
        [
            "Quelle type de boucle envisagez-vous pour le code de cette fonction ?",
            ["for cle in dico.keys()","for valeur in dico.values()", "for (cle, valeur) in dico.items()", " je n'utiliserai aucune boucle"],[2],
        ],
        shuffle = False)}}


!!! question "Écrire le code de cette fonction"

    {{ IDE() }}


__3.__ On souhaite se doter d'une fonction `nombre(zoo, animal)` qui prend un zoo en paramètre ainsi que le nom d'un animal, et qui renvoie le nombre de représentants de cet animal dans le zoo.

Par exemple, on veut avoir le fonctionnement suivant :

```pycon
>>> nombre(zoo_beauval, 'panda')
2
>>> nombre(zoo_la_fleche, 'panda')
0
```

!!! question ""

    {{ multi_qcm(
        [
            "Quelle type de boucle envisagez-vous pour le code de cette fonction ?",
            ["for cle in dico.keys()","for valeur in dico.values()", "for (cle, valeur) in dico.items()", " je n'utiliserai aucune boucle"],[1,4],
        ],
        shuffle = False)}}

    ??? danger "Explication"

        En effet, il y a deux façons de coder cette fonction : soit avec une boucle du type `for cle in dico.keys()`, soit avec un test du type `if cle in dico.keys()`.

!!! question "Écrire le code de cette fonction"

    {{ IDE() }}




## Exercice 6 : le clavier d'ordinateur (_pour les rapides_)

On peut voir un clavier comme un tableau à deux dimensions dans lequel chaque case contient un caractère.

Ainsi, la partie principale d'un clavier AZERTY peut être vue comme le tableau suivant

```python
azerty = [['a', 'z', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p'],
 ['q', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm'],
 ['<', 'w', 'x', 'c', 'v', 'b', 'n', ',', ';', ':']]
```

Notre objectif est d'écrire une fonction `distance_touches` calculant la distance entre les touches de deux caractères sur un clavier.

!!! question "Question 1"
    Écrire une fonction `inverse_clavier` prenant en paramètre un tableau à deux dimensions représentant une disposition de clavier et renvoyant un dictionnaire dont les clés sont les caractères et les valeurs les coordonnées de la touche correspondante.
    
    {{ IDE('script_exo8') }}


!!! question "Question 2"
    En déduire une fonction `distance_touches` qui prend en paramètres deux caractères et un tableau à deux dimensions représentant une disposition de clavier et qui renvoie la distance entre les touches correspondantes. 
    
    _N'hésitez pas à utiliser le théorème de Pythagore !_  On prendra comme unité de distance le côté d'une touche et on arrondira à 2 chiffres après la virgule.

    {{ IDE('script_exo9') }}

## Exercice 7 : introduction aux bases de données

On considère la liste des 12 athlètes finalistes de l'épreuve féminine de saut en longueur aux Jeux Olympiques de Paris 2024. 

Pour chaque athlète, on stocke son nom, son pays d'origine et sa performance (en mètres) dans un dictionnaire. Puis on réunit tous ces "petits" dictionnaires dans un tableau :


{{ IDE('sautLongueur') }}



Le fait de stocker les informations de chaque athlète dans un dictionnaire rend **plus compréhensible la recherche d'informations** dans ce tableau, comme illustré ci-dessous.

**1.** Que va contenir le tableau `tab1` ?

```python
tab1 = [athlete["Nom"] for athlete in Finale_SautLongueur_Femmes if athlete["Pays"] == 'Nigeria']
```

{{ terminal() }}

**2.** Que va contenir le tableau `tab2` ?

```python
tab2 = [athlete["Nom"] for athlete in Finale_SautLongueur_Femmes if athlete["Performance"] >= 6.90]
```

{{ terminal() }}
