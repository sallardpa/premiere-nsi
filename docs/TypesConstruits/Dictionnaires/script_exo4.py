def taille(dico, nom):
    """
    Entrées :
        dico : un dictionnaire où clé = nom du pokemon et valeur = (taille,poids)
        nom : une chaine de caractères correspondant à une clé du dictionnaire
    Sortie : un nombre égal à la taille du pokemon
    """
    # Coder votre fonction ici
    
    
# Cette assertion permettra de vérifier si votre fonction renvoie bien la valeur voulue sur deux exemples.
assert taille(exemple_pokemons, 'Abo') == 1, "problème 1"
assert taille(exemple_pokemons, 'Jungko') == 5, "problème 2"