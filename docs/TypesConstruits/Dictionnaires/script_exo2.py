def mystere(pokemons):
    """
    Entrées :
        pokemons : un dictionnaire où clé = nom du pokemon et valeur = (taille,poids)
    Sortie : à vous de trouver !
    """
    grand = None
    taille_max = None
    for (nom, (taille, poids)) in pokemons.items():
        if taille_max is None or taille > taille_max:
            taille_max = taille
            grand = nom
    return (grand, taille_max)