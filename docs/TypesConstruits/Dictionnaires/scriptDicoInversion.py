def inversion(dico):
    newdico = ... # initialisation d'un dictionnaire vide
    ... # plusieurs lignes ici
    return ...

# un test
francais_anglais = {"chien": "dog", "vache": "cow", "cochon": "pig", "canard": "duck"}
print(inversion(francais_anglais))