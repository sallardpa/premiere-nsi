def le_plus_leger(pokemons):
    """
    Entrées :
        pokemons : un dictionnaire où clé = nom du pokemon et valeur = (taille,poids)
    Sortie : la fonction renvoie un couple composé du nom du plus léger ainsi que son poids
    """
    # Coder votre fonction ici

# Cette assertion permettra de vérifier si votre fonction renvoie bien la valeur voulue sur un exemple.
assert le_plus_leger(exemple_pokemons) == ('Abo', 7)