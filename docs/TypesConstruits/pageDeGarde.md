# Représentation des données : les types construits

Dans cette partie, sont traités les thèmes :

* les tableaux (partie 1 et 2) :postbox:
* les p-uplets (:flag_gb: :flag_us: _tuples_) :dango:
* les dictionnaires :bookmark_tabs:
