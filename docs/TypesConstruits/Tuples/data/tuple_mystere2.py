#--- HDR ---#
myFriend = ("Joan", "Clarke", 1917, 1996)
#--- HDR ---#
# dans une partie cachée du code, un tuple myFriend a été créé.

# parcours version 1
for k in range(...) :
    print(...) 

# parcours version 2
for elt in ... :
    print(...)