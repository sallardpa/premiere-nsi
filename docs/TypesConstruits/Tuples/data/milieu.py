def milieu(pointA, pointB):
    """ Calcul les coordonnées du milieu des deux points A et B.
    Entrée : deux tuples à deux éléments 
    Sortie : un tuple"""
    (xA,yA) = pointA
    ...
    xM = ...
    yM = ...
    pointM = ...
    return pointM

# un test
A = (3, 12)
B = (-4, 5)
I = milieu(A,B) # on récupère le tuple renvoyé par la fonction
print("le milieu des points", A, " et ", B, " est le point ", I)
assert I == (-0.5,8.5), "problème de code"