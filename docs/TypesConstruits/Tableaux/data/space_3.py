# dans le code précédent, effacer les parties UPDATE et DRAW
# puis copier le contenu de ce fichier
# et le coller en-dessous la fonction tirs_deplacement(tirs_liste):

# initialisation des ennemis
ennemis_liste = []
# ce tableau contiendra les coordonnées des différents ennemis

def ennemis_creation(ennemis_liste):
    """création aléatoire des ennemis"""

    # un ennemi par seconde
    if (pyxel.frame_count % 30 == 0) :
        x_alea = random.randint(0, 120) # abscisse x aléatoire entre 0 et 120
        ennemi = ... # position initiale de l'ennemi à l'abscisse x_alea sur la ligne du haut (y = 0)
        ... # ajouter cet ennemi au tableau ennemis_liste
    return ennemis_liste


def ennemis_deplacement(ennemis_liste):
    """déplacement des ennemis vers le haut et suppression s'ils sortent du cadre"""

    for ... in ... : # compléter ici pour parcourir la liste des ennemis
        ennemi[1] += 1 # on augmente l'ordonnée y pour aller vers le bas
        if  ennemi[1] > 128 : # si l'ennemi sort du cadre, on l'enlève du tableau ennemis_liste
            ennemis_liste.remove(ennemi)
    return ennemis_liste


# =========================================================
# == UPDATE
# =========================================================
def update():
    """mise à jour des variables (30 fois par seconde)"""

    global vaisseau_x, vaisseau_y, tirs_liste, ennemis_liste

    # mise à jour de la position du vaisseau
    vaisseau_x, vaisseau_y = vaisseau_deplacement(vaisseau_x, vaisseau_y)

    # creation des tirs en fonction de la position du vaisseau
    tirs_liste = tirs_creation(vaisseau_x, vaisseau_y, tirs_liste)

    # mise a jour des positions des tirs
    tirs_liste = tirs_deplacement(tirs_liste)

    # creation des ennemis
    ennemis_liste = ennemis_creation(ennemis_liste)

    # mise a jour des positions des ennemis
    ennemis_liste = ennemis_deplacement(ennemis_liste)    


# =========================================================
# == DRAW
# =========================================================
def draw():
    """création des objets (30 fois par seconde)"""

    # vide la fenetre (clear screen)
    pyxel.cls(0)

    # vaisseau (carre 8x8, couleur 1 = bleue)
    pyxel.rect(vaisseau_x, vaisseau_y, 8, 8, 1)

    # tirs (rectangle 1x4, couleur 10 = jaune)
    for tir in tirs_liste:
        pyxel.rect(tir[0], tir[1], 1, 4, 10)

    # ennemis (carre 8x8, couleur 14 = rose)
    for ennemi in ennemis_liste:
        pyxel.rect(ennemi[0], ennemi[1], 8, 8, 14)        

pyxel.run(update, draw)

