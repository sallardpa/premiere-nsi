def carré(tab) :
    tab_carre = [0] * len(tab) 
    for k in range(len(tab)) :
        tab_carre[k] = tab[k] ** 2
    return tab_carre