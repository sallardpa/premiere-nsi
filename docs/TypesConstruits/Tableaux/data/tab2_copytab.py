# ici, les variables tab_a et tab_b sont des listes (de type list)
tab_a = [4, 5, 7]
tab_b = tab_a
tab_a[0] = 12 # on modifie la première valeur du tableau tab_a
print(tab_a)
print(tab_b)