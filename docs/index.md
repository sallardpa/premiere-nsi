# Cours de Première NSI

!!! info ""
    Les pages de ce site sont sans doute plus lisibles en mode jour (_changer le mode en cliquant sur l'icône_ soleil :sunny: ou lune :last_quarter_moon_with_face:), mais vous êtes libre de sélectionner le mode que vous préférez :relaxed:.

## 1. Contenu du cours :cinema:

Ce cours de Première NSI est volontairement limité au strict nécessaire. 

Il doit susciter un certain nombre de questions de votre part : votre professeur est là pour vous guider et vous apporter des éléments de réponse.

Pour des approfondissements, voici quelques sites d'autres enseignants de NSI : 

* [Gilles Lassus](https://glassus.github.io/premiere_nsi/){target="_blank"}  
* [Mireille Coilhac](https://mcoilhac.forge.apps.education.fr/site-nsi/){target="_blank"} 


## 2. Mode d'emploi :trident:

Certaines parties du cours sont illustrées par des codes écrits en langage Python, comme ci-dessous par exemple. 
```python
prenom = 'Esmeralda' # à remplacer par votre prénom
print('Bonjour ' + prenom)
```

Vous pouvez ensuite le copier dans l'éditeur ci-dessous et l'exécuter en appuyant sur le bouton _Lancer_ :arrow_forward:.

!!! note ""
    {{IDE()}}


## 3. Outils indispensables :hammer:

!!! info "Outils indispensables"

    * Pour la programmation en Python, vous avez besoin d'un "éditeur Python" (_IDE_) : [Thonny](https://thonny.org/){target="_blank"} est l'outil le plus simple, mais vous pouvez aussi utiliser [Edupython](https://edupython.tuxfamily.org/#téléchargement){target="_blank"}, Spyder, VSCode/VSCodium, etc.

    * La plupart des exercices sont présentés dans des "carnets Jupyter" (_Jupyter Notebook_), comme [celui-ci](Programmation/1_BasesPython/IntroJupyter.ipynb): 

        - il faut le télécharger en faisant un ***clic droit*** sur le lien, puis ***Enregistrer le lien sous...***
        - vous pouvez le lire en l'ouvrant (_Fichier/Ouvrir_) sur le site [Basthon](https://notebook.basthon.fr/){target="_blank"} ou bien avec une application Jupyter installée sur votre PC.

!!! tip "Conseil"
    Edupyter, disponible [ici](https://www.edupyter.net/){target="_blank"}, donne accès à la fois à Thonny et à Jupyter :+1:. 

    Il s'installe sur un PC Windows **sans les droits administrateurs**.  Il se lance en cliquant sur l'icône qui apparaît à côté de l'horloge (voir [schéma](https://raw.githubusercontent.com/edupyter/documentation/main/edupyter-help.png){target="_blank"}).





